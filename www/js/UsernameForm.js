function UsernameForm (application, options) {

    const username_input = Form_TextInput({
        name: 'username',
        label: 'Username',
        required: true,
        value: application.variables.session_user.username,
    })

    const current_password_input = Form_TextInput({
        next: true,
        type: 'password',
        name: 'current_password',
        label: 'Current password',
        required: true,
    })

    const form = Form({
        title: 'Change username',
        action: 'username/',
        logout: options.logout,
        items: [
            username_input.element,
            current_password_input.element,
            Form_Button({ text: 'Save' }),
        ],
        submit () {

            const username = username_input.get_value()
            if (username === null) return

            const current_password = current_password_input.get_value()
            if (current_password === null) return

            return {
                username: username,
                current_password: current_password,
            }

        },
        done (response, params) {

            if (response === true) {
                application.variables.session_user.username = params.username
                options.done()
                return
            }

            if (response === 'CURRENT_PASSWORD_INVALID') {
                form.error('Invalid current password')
                return
            }

        },
    })

    return {
        destroy: form.destroy,
        element: form.element,
        focus: username_input.focus,
    }

}
