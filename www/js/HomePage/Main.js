function HomePage_Main (application, options) {

    let period = null

    let abort = () => {}

    const top = Top(application, {
        logout () {
            abort()
            options.logout()
        },
    })

    const period_select = PeriodSelect_Main(application, {
        logout () {
            top.destroy()
            options.logout()
        },
        update (filter) {
            abort()
            abort = Period_Main({
                filter: filter,
                logout: options.logout,
                done (new_period) {
                    const scrollTop = element.scrollTop
                    if (period !== null) {
                        period.destroy()
                        element.removeChild(period.element)
                    }
                    period = new_period
                    element.append(period.element)
                    element.scrollTop = scrollTop
                },
            })
        },
    })

    const element = Element({ className: 'HomePage_Main' }, [
        top.element,
        period_select.element,
    ])

    return {
        element,
        focus: period_select.init,
    }

}
