function Form_TextInput (options) {

    const input = Element('input', {
        className: 'Form_TextInput-input',
        name: options.name,
    })
    if (options.type !== undefined) input.type = options.type
    if (options.value !== undefined) input.value = options.value

    return {
        element: Element({
            className: 'Form_TextInput' + (options.next ? ' Next-medium' : ''),
        }, [options.label + ':', input]),
        focus () {
            input.focus()
        },
        get_value () {
            const value = (() => {
                const value = input.value
                if (options.type === 'password') return value
                return CollapseSpaces(value)
            })()
            if (options.required && value === '') {
                input.value = ''
                input.focus()
                return null
            }
            return value
        },
    }

}
