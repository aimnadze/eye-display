function LoginPage_Main (application, options) {

    const form = LoginPage_Form(application, options)

    return {
        element: Element({
            className: 'LoginPage_Main',
            style: { backgroundImage: 'url(img/login.svg)' },
        }, [
            Element({ className: 'LoginPage_Main-aligner' }),
            Element({ className: 'LoginPage_Main-content' }, [form.element]),
        ]),
        focus: form.focus,
    }

}
