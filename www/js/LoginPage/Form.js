function LoginPage_Form (application, options) {

    function error (text) {
        error_element = Form_Error({ text: text })
        form.insertBefore(error_element, username_input.element)
    }

    let abort = () => {}

    let error_element = null

    const username_input = Form_TextInput({
        name: 'username',
        label: 'Username',
        required: true,
    })

    const password_input = Form_TextInput({
        next: true,
        type: 'password',
        name: 'password',
        label: 'Password',
        required: true,
    })

    const form = Element('form', {
        className: 'LoginPage_Form',
        onsubmit (e) {

            abort()
            e.preventDefault()

            const username = username_input.get_value()
            if (username === null) return

            const password = password_input.get_value()
            if (password === null) return

            const request = PostJson('api/login/', BuildQuery({
                username: username,
                password: password,
            }), () => {}, response => {

                if (response === true) {
                    application.variables.session_user = {
                        username: username,
                    }
                    options.login()
                    return
                }

                if (response === 'LOGIN_INVALID') {
                    error('Invalid username or password')
                    abort = () => {
                        form.removeChild(error_element)
                    }
                    return
                }

            })

            abort = () => {
                request.abort()
            }

        },
    }, [
        Element({ className: 'LoginPage_Form-icon' }, [
            Element({
                className: 'LoginPage_Form-icon-content',
                style: { backgroundImage: 'url(img/icon/32.svg)' },
            }),
        ]),
        Element([
            Element({ className: 'LoginPage_Form-title' }, ['Eye']),
        ]),
        Element({ className: 'LoginPage_Form-subtitle' }, [
            'Reachability Monitoring',
        ]),
        username_input.element,
        password_input.element,
        Form_Button({ text: 'Log in' }),
    ])

    return {
        element: form,
        focus: username_input.focus,
    }

}
