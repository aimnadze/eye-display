function FormatTime (time) {
    const date = new Date(time)
    return MonthNameShort[date.getUTCMonth()] + ' ' +
        TwoDigitPad(date.getUTCDate()) + ' ' +
        TwoDigitPad(date.getUTCHours()) + ':' +
        TwoDigitPad(date.getUTCMinutes())
}
