function TopButton (options) {
    return Element('button', {
        className: 'TopButton',
        onclick: options.click,
        style: { backgroundImage: 'url(img/white/' + options.icon + '.svg)' },
    }, [
        Element({ className: 'TopButton-tip' }, [
            Element({ className: 'TopButton-tip-arrow' }),
            options.text,
        ]),
    ])
}
