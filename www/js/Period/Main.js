function Period_Main (options) {

    const filter = options.filter

    const request = PostJson('api/fetch/', BuildQuery({
        from: Math.floor(filter.from / 1000),
        to: Math.floor(filter.to / 1000),
    }), () => {}, response => {

        if (response === 'LOG_IN') {
            options.logout()
            return
        }

        const urls = Object.create(null)
        response.checks.forEach(check => {
            urls[check.url] = {}
            response.agents.forEach(agent => {
                urls[check.url][agent.id] = []
            })
        })
        response.reads.forEach(item => {
            const agent_map_items = urls[item.url]
            if (agent_map_items === undefined) return
            agent_map_items[item.agent_id].push(item)
        })

        options.done({
            element: Element({
                className: 'Period_Main',
            }, Object.keys(urls).map(url => (
                Period_Item(response, filter, url, urls[url])
            ))),
            destroy () {
                request.abort()
            },
        })

    })

    return () => {
        request.abort()
    }

}
