function Period_Item (response, filter, url, agents) {
    return Element({ className: 'Period_Item' }, [
        Element({ className: 'Period_Item-title' }, [url]),
        Element({
            className: 'Period_Item-content',
        }, response.agents.map((agent, index) => (
            Period_ItemLine(filter, agent, agents[agent.id], index)
        ))),
    ])
}
