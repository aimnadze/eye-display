function Period_Chart (filter, reads) {

    const width_ratio = (() => {
        let width_ratio = (1000 * 60) / (filter.to - filter.from)
        while (width_ratio < 0.005) width_ratio *= 2
        return width_ratio
    })()

    return Element({ className: 'Period_Chart' }, reads.map(read => {

        const left_ratio = (read.time * 1000 - filter.from) / (filter.to - filter.from)

        return Element({
            className: 'Period_Chart-item',
            style: {
                left: left_ratio * 100 + '%',
                width: 'calc(' + (width_ratio * 100) + '% + 1px)',
            },
        }, [
            Element({ className: 'Period_Chart-item-fail' }),
            Element({
                className: 'Period_Chart-item-success',
                style: { height: read.result_success * 100 + '%' },
            }),
        ])

    }))

}
