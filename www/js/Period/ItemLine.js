function Period_ItemLine (filter, agent, reads, index) {
    return Element({
        className: 'Period_ItemLine' + (index === 0 ? '' : ' next'),
    }, [
        Element({ className: 'Period_ItemLine-name' }, [agent.name]),
        Period_Chart(filter, reads),
    ])
}
