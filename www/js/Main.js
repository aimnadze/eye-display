window.Main = variables => {

    function load (page) {
        current_page = page
        element.append(page.element)
        page.focus()
    }

    function showHomePage () {
        const home_page = HomePage_Main(application, {
            logout () {
                unload()
                showLoginPage()
            },
        })
        load(home_page)
    }

    function showLoginPage () {
        const login_page = LoginPage_Main(application, {
            login () {
                unload()
                showHomePage()
            },
        })
        load(login_page)
    }

    function unload () {
        element.removeChild(current_page.element)
    }

    let current_page
    const time_difference = Date.now() - variables.time

    const items = {
        scroll: [],
    }

    const application = {
        variables: variables,
        add (name, item) {
            items[name].push(item)
        },
        resource_url (path) {
            return variables.site_base + path
        },
        show_form (form) {

            function hide () {
                element.removeChild(dialog)
            }

            const dialog = Dialog({
                content: [form.element],
                close () {
                    hide()
                    form.destroy()
                },
            })
            element.append(dialog)
            form.focus()
            return hide

        },
        time_now () {
            return Date.now() - time_difference
        },
    }

    const element = Element({
        className: 'Main',
        onscroll () {
            items.scroll.forEach(item => {
                item(element)
            })
        },
    })

    document.body.append(element)
    if (variables.session_user) showHomePage()
    else showLoginPage()

}
