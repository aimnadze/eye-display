function PostJson (url, formData, errorCallback, loadCallback) {
    const request = new XMLHttpRequest
    request.open('post', url)
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
    request.responseType = 'json'
    request.send(formData)
    request.onerror = errorCallback
    request.onload = () => {
        if (request.status !== 200) {
            errorCallback()
            return
        }
        loadCallback(request.response)
    }
    return request
}
