function CollapseSpaces (string) {
    return string.replace(/\s+/g, ' ').replace(/^\s*(.*?)\s*$/, '$1')
}
