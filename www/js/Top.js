function Top (application, options) {

    let abort = () => {}

    return {
        element: Element({
            className: 'Top',
            style: { backgroundImage: 'url(img/top.svg)' },
        }, [
            Element({ className: 'Top-title' }, ['Eye']),
            Element({ className: 'Top-right' }, [
                TopButton({
                    icon: 'user',
                    text: 'Change username',
                    click () {
                        const hide = application.show_form(UsernameForm(application, {
                            done () {
                                hide()
                            },
                            logout () {
                                hide()
                                abort()
                                options.logout()
                            },
                        }))
                    },
                }),
                TopButton({
                    icon: 'key',
                    text: 'Change password',
                    click () {
                        const hide = application.show_form(PasswordForm({
                            done () {
                                hide()
                            },
                            logout () {
                                hide()
                                abort()
                                options.logout()
                            },
                        }))
                    },
                }),
                TopButton({
                    icon: 'arrow-right-out',
                    text: 'Log out',
                    click () {
                        abort()
                        const request = PostJson('api/logout/', '', () => {}, response => {
                            if (response === true) options.logout()
                        })
                        abort = () => {
                            request.abort()
                        }
                    },
                }),
            ]),
        ]),
        destroy () {
            abort()
        },
    }

}
