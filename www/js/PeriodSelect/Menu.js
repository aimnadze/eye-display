function PeriodSelect_Menu (application, options) {

    function add (item_options) {

        function deselect () {
            destroy()
            button.classList.remove('active')
        }

        function select () {
            destroy = item_options.update(options.update)
            button.classList.add('active')
            deselect_selected = deselect
            button_node.nodeValue = item_options.text
            hide()
        }

        let destroy

        const button = Element('button', {
            className: 'PeriodSelect_Menu-item',
            onclick () {
                deselect_selected()
                select()
            },
        }, [item_options.text])

        menu_element.append(button)
        items.push(select)

    }

    function hide () {
        visible = false
        button.classList.remove('active')
        menu_element.classList.add('hidden')
        removeEventListener('focus', window_focus, true)
        removeEventListener('mousedown', window_mousedown)
    }

    function window_focus (e) {
        const target = e.target
        if (target === window || target === document) return
        if (element.contains(target)) return
        hide()
    }

    function window_mousedown (e) {
        if (element.contains(e.target)) return
        hide()
    }

    let deselect_selected
    let visible = false
    const items = []

    const button_node = document.createTextNode('button1')

    const button = Element('button', {
        className: 'PeriodSelect_Menu-button',
        style: {
            backgroundImage: 'url(' + application.resource_url('img/dark/point-bottom.svg') + ')',
        },
        onclick () {

            if (visible) {
                hide()
                return
            }

            visible = true
            button.classList.add('active')
            menu_element.classList.remove('hidden')
            addEventListener('focus', window_focus, true)
            addEventListener('mousedown', window_mousedown)

        },
    }, [button_node])

    const menu_element = Element({
        className: 'PeriodSelect_Menu-menu hidden',
    })
    add(PeriodSelect_OneHour(application))
    add(PeriodSelect_Days(application, 1))
    add(PeriodSelect_Days(application, 7))

    const element = Element({ className: 'PeriodSelect_Menu' }, [
        button,
        menu_element,
    ])

    return {
        element,
        init: items[0],
    }

}
