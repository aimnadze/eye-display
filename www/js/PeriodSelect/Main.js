function PeriodSelect_Main (application, options) {

    const right_element = Element({ className: 'PeriodSelect_Main-right' })

    const menu = PeriodSelect_Menu(application, {
        update (filter) {
            while (right_element.lastChild) {
                right_element.lastChild.remove()
            }
            right_element.append(Element({
                className: 'PeriodSelect_Main-right-time',
            }, [FormatTime(filter.from)]))
            right_element.append(Element({
                className: 'PeriodSelect_Main-right-separator',
            }, [' - ']))
            right_element.append(Element({
                className: 'PeriodSelect_Main-right-time',
            }, [FormatTime(filter.to)]))
            options.update(filter)
        },
    })

    const left_element = Element({ className: 'PeriodSelect_Main-left' })
    left_element.append(menu.element)

    const element = Element({ className: 'PeriodSelect_Main' }, [
        left_element,
        right_element,
    ])

    application.add('scroll', main_element => {
        if (main_element.scrollTop === 0) element.classList.remove('scroll')
        else element.classList.add('scroll')
    })

    return {
        element,
        init: menu.init,
    }

}
