function PeriodSelect_Days (application, number) {
    return {
        text: number + 'd',
        update (render) {

            function next () {

                const to_date = new Date(application.time_now())
                to_date.setUTCMilliseconds(0)
                to_date.setUTCSeconds(0)

                const from_date = new Date(to_date.getTime())
                from_date.setUTCDate(from_date.getUTCDate() - number)

                timeout = setTimeout(next, 1000 * 60 * 60)

                render({
                    from: from_date.getTime(),
                    to: to_date.getTime(),
                })

            }

            let timeout
            next()

            return () => {
                clearTimeout(timeout)
            }

        },
    }
}
