<?php

class Table {

    private $name;
    private $where;

    function __construct ($name, $where = []) {
        $this->name = $name;
        $this->where = $where;
    }

    function count ($where = []) {
        return (int)mysqli_value(mysqli(),
            'select count(*)' .
            ' from `' . mysqli()->real_escape_string($this->name) . '`' .
            ' ' . mysqli_where(mysqli(), $this->where($where)));
    }

    function delete ($where) {
        return mysqli_delete(mysqli(), $this->name, $this->where($where));
    }

    function get ($id) {
        if ($id === 0) return;
        return mysqli_select_single_assoc(
            mysqli(), $this->name, compact('id'));
    }

    function insert ($values) {
        return mysqli_insert(mysqli(), $this->name, $values);
    }

    function iterate ($where, $callback) {
        $last_id = null;
        while (true) {

            $item_where = $this->where($where);
            if ($last_id !== null) $item_where[] = "id > $last_id";

            $last_id = null;

            foreach ($this->query_assoc($item_where, 'id', [
                'offset' => 0,
                'limit' => 2000,
            ]) as $row) {
                $last_id = $row['id'];
                $callback($row);
            }

            if ($last_id === null) break;

        }
    }

    function query_assoc ($where, $order_by = null, $page = null) {
        return mysqli_select_query_assoc(mysqli(),
            $this->name, $this->where($where), $order_by, $page);
    }

    function single_assoc ($where, $order_by = null) {
        return mysqli_select_single_assoc(
            mysqli(), $this->name, $this->where($where), $order_by);
    }

    function subtable ($where) {
        return new Table($this->name, $this->where($where));
    }

    function truncate () {
        mysqli_safe_query(mysqli(),
            'truncate `' . mysqli()->real_escape_string($this->name) . '`');
    }

    function update ($values, $where) {
        return mysqli_update(mysqli(), $this->name,
            $values, $this->where($where));
    }

    function where_conditions () {
        return mysqli_where_conditions(mysqli(), $this->where);
    }

    private function where ($where) {
        return array_merge($this->where, $where);
    }

}
