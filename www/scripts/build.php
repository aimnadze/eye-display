#!/usr/bin/php
<?php

chdir(__DIR__);
include_once '../lib/init-cli.php';

$compressed_css_file = '../compressed.css';
$compressed_js_file = '../compressed.js';
$revisions_file = '../f/revisions.php';

$compressed_css = file_get_contents($compressed_css_file);
$compressed_js = file_get_contents($compressed_js_file);

$revisions = revisions();

system('./compress-css.js');
system('./compress-js.js');

clearstatcache();

$changed = false;
if (file_get_contents($compressed_css_file) != $compressed_css) {
    $revisions['compressed.css']++;
    $changed = true;
}
if (file_get_contents($compressed_js_file) != $compressed_js) {
    $revisions['compressed.js']++;
    $changed = true;
}

if ($changed) {

    $values = '';
    foreach ($revisions as $key => $value) {
        $values .= "        '$key' => $value,\n";
    }

    $content =
        "<?php\n\n" .
        "function revisions () {\n" .
        "    return [\n" .
        $values .
        "    ];\n" .
        "}\n";
    file_put_contents($revisions_file, $content);

}

echo "Done\n";
