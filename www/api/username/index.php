<?php

include_once '../lib/init-valid.php';

$username = str_collapse_spaces(request_string('username'));
$current_password = request_string('current_password');

if ($username === '') respond('BAD_REQUEST');

$admin = Admin\get();

if (!password_verify($current_password, $admin['password_hash'])) {
    respond('CURRENT_PASSWORD_INVALID');
}

Admin\set([
    'username' => $username,
    'password_hash' => $admin['password_hash'],
]);

respond(true);
