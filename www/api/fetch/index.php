<?php

include_once '../lib/init-valid.php';

session_commit();

$agents = db()->Agent->query_assoc([], 'name');

respond([
    'agents' => array_map(function ($agent) {
        return [
            'id' => (int)$agent['id'],
            'name' => $agent['name'],
        ];
    }, $agents),
    'checks' => array_map(function ($check) {
        return [
            'url' => $check['url'],
        ];
    }, db()->Check->query_assoc([])),
    'reads' => array_map(function ($read) {
        return [
            'agent_id' => (int)$read['agent_id'],
            'time' => (int)$read['insert_time'],
            'url' => $read['url'],
            'result_success' => (float)$read['result_success'],
        ];
    }, Read\index($agents, request_filter())),
]);
