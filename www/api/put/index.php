<?php

include_once '../../lib/init-web.php';

$request = fix_type(json_decode(file_get_contents('php://input'), true), [
    'username' => 'string',
    'password' => 'string',
    'minute' => [
        'time' => 'int',
        'urls' => [
            [
                'url' => 'string',
                'result' => [
                    'error' => 'string',
                    'status_code' => 'int',
                    'headers' => [],
                ],
            ],
        ],
    ],
]);

$agent = db()->Agent->single_assoc([
    'name' => $request['username'],
]);
if ($agent === null || !password_verify($request['password'], $agent['password_hash'])) {
    respond('INVALID_LOGIN');
}

$minute = $request['minute'];

$checks = db()->Check->query_assoc([]);

$url_map_check = [];
foreach ($checks as $check) $url_map_check[$check['url']] = $check;

foreach ($minute['urls'] as $item) {

    $url = $item['url'];
    if (!array_key_exists($url, $url_map_check)) continue;

    $result = $item['result'];
    $check = $url_map_check[$url];

    $values = [
        'agent_id' => $agent['id'],
        'insert_time' => $minute['time'],
        'url' => $url,
        'result_error' => $result['error'],
        'result_status_code' => $result['status_code'],
        'result_headers' => json_encode($result['headers']),
        'expect_status_code' => $check['expect_status_code'],
        'expect_headers' => $check['expect_headers'],
        'result_success' => success($item, $check),
    ];

    for ($i = 0; $i < 16; $i++) {
        $values["insert_minute_$i"] = floor($minute['time'] / 60 / pow(2, $i));
    }

    db()->Read->insert($values);

}

respond(array_map(function ($check) {
    return $check['url'];
}, $checks));
