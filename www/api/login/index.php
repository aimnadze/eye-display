<?php

include_once '../../lib/init-web.php';

if (array_key_exists('user', $_SESSION)) respond(true);

$username = str_collapse_spaces(request_string('username'));
$password = request_string('password');

$admin = Admin\get();

if ($username !== $admin['username'] ||
    !password_verify($password, $admin['password_hash'])) {

    respond('LOGIN_INVALID');

}

$_SESSION['user'] = true;

respond(true);
