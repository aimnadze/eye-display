<?php

include_once '../lib/init-valid.php';

$current_password = request_string('current_password');
$new_password = request_string('new_password');

if ($new_password === '') respond('BAD_REQUEST');

$admin = Admin\get();

if (!password_verify($current_password, $admin['password_hash'])) {
    respond('CURRENT_PASSWORD_INVALID');
}

Admin\set([
    'username' => $admin['username'],
    'password_hash' => password_hash($new_password, PASSWORD_DEFAULT),
]);

respond(true);
