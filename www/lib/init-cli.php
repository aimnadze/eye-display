<?php

include_once __DIR__ . '/init.php';

if (php_sapi_name() !== 'cli') die('ERROR: CLI required.');

include_once __DIR__ . '/../f/ensure_tables.php';
include_once __DIR__ . '/../f/render_inkscape_svg_files.php';
include_once __DIR__ . '/../f/unit_test.php';
include_once __DIR__ . '/../f/InformationSchema/Column/OnTable/index.php';
include_once __DIR__ . '/../f/InformationSchema/Index/build.php';
include_once __DIR__ . '/../f/InformationSchema/Index/OnTable/index.php';
include_once __DIR__ . '/../f/InformationSchema/Table/get.php';
include_once __DIR__ . '/../f/Table/addColumns.php';
include_once __DIR__ . '/../f/Table/columnDefinition.php';
include_once __DIR__ . '/../f/Table/create.php';
include_once __DIR__ . '/../f/Table/dropPrimaryKey.php';
include_once __DIR__ . '/../f/Table/editColumn.php';
include_once __DIR__ . '/../f/Table/ensure.php';
include_once __DIR__ . '/../f/Table/ensureIndices.php';
