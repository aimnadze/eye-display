(function () {
function BuildQuery (params) {

    function scan (params, prefix) {
        for (const i in params) {
            const value = (() => {
                const value = params[i]
                if (typeof value === 'boolean') return value ? '1' : ''
                return value
            })()
            if (typeof value === 'object') {
                scan(value, prefix + i + '_')
                continue
            }
            if (value === null) continue
            flat[prefix + i] = value
        }
    }

    const flat = {}
    scan(params, '', '')

    const array = []
    for (const i in flat) {
        array.push(i + '=' + encodeURIComponent(flat[i]))
    }
    return array.join('&')

}
;
function CollapseSpaces (string) {
    return string.replace(/\s+/g, ' ').replace(/^\s*(.*?)\s*$/, '$1')
}
;
function Dialog (options) {
    const element = Element({
        className: 'Dialog',
        onclick (e) {
            if (e.target === element) options.close()
        },
    }, [
        Element({ className: 'Dialog-aligner' }),
        Element({ className: 'Dialog-content' }, options.content),
    ])
    return element
}
;
function Element (...args) {

    const name = typeof args[0] === 'string' ? args.shift() : 'div'
    const properties = args[0] instanceof Array ? {} : args.shift()
    const content = args[0] instanceof Array ? args.shift() : []

    const element = document.createElement(name)
    for (const i in properties) {
        if (i === 'style') {
            Object.assign(element.style, properties[i])
            continue
        }
        element[i] = properties[i]
    }
    element.append(...content)
    return element

}
;
function Form (options) {

    let abort = () => {}

    let error_element = null

    const element = Element('form', {
        onsubmit (e) {

            abort()
            e.preventDefault()

            const params = options.submit()
            if (params === undefined) return

            const request = PostJson('api/' + options.action,
                BuildQuery(params), () => {}, response => {

                if (response === 'LOG_IN') {
                    options.logout()
                    return
                }

                options.done(response, params)

            })

            abort = () => {
                request.abort()
            }

        },
    }, [
        Page_H1({ text: options.title }),
        ...options.items,
    ])

    return {
        element,
        destroy () {
            abort()
        },
        error (text) {
            error_element = Form_Error({ text: text })
            element.insertBefore(error_element, element.childNodes[1])
            abort = () => {
                element.removeChild(error_element)
                abort = () => {}
            }
        },
    }

}
;
function Form_Button (options) {
    return Element('button', { className: 'Form_Button' }, [options.text])
}
;
function Form_Error (options) {
    return Element({ className: 'Form_Error' }, [options.text])
}
;
function Form_TextInput (options) {

    const input = Element('input', {
        className: 'Form_TextInput-input',
        name: options.name,
    })
    if (options.type !== undefined) input.type = options.type
    if (options.value !== undefined) input.value = options.value

    return {
        element: Element({
            className: 'Form_TextInput' + (options.next ? ' Next-medium' : ''),
        }, [options.label + ':', input]),
        focus () {
            input.focus()
        },
        get_value () {
            const value = (() => {
                const value = input.value
                if (options.type === 'password') return value
                return CollapseSpaces(value)
            })()
            if (options.required && value === '') {
                input.value = ''
                input.focus()
                return null
            }
            return value
        },
    }

}
;
function FormatTime (time) {
    const date = new Date(time)
    return MonthNameShort[date.getUTCMonth()] + ' ' +
        TwoDigitPad(date.getUTCDate()) + ' ' +
        TwoDigitPad(date.getUTCHours()) + ':' +
        TwoDigitPad(date.getUTCMinutes())
}
;
function HomePage_Main (application, options) {

    let period = null

    let abort = () => {}

    const top = Top(application, {
        logout () {
            abort()
            options.logout()
        },
    })

    const period_select = PeriodSelect_Main(application, {
        logout () {
            top.destroy()
            options.logout()
        },
        update (filter) {
            abort()
            abort = Period_Main({
                filter: filter,
                logout: options.logout,
                done (new_period) {
                    const scrollTop = element.scrollTop
                    if (period !== null) {
                        period.destroy()
                        element.removeChild(period.element)
                    }
                    period = new_period
                    element.append(period.element)
                    element.scrollTop = scrollTop
                },
            })
        },
    })

    const element = Element({ className: 'HomePage_Main' }, [
        top.element,
        period_select.element,
    ])

    return {
        element,
        focus: period_select.init,
    }

}
;
function LoginPage_Form (application, options) {

    function error (text) {
        error_element = Form_Error({ text: text })
        form.insertBefore(error_element, username_input.element)
    }

    let abort = () => {}

    let error_element = null

    const username_input = Form_TextInput({
        name: 'username',
        label: 'Username',
        required: true,
    })

    const password_input = Form_TextInput({
        next: true,
        type: 'password',
        name: 'password',
        label: 'Password',
        required: true,
    })

    const form = Element('form', {
        className: 'LoginPage_Form',
        onsubmit (e) {

            abort()
            e.preventDefault()

            const username = username_input.get_value()
            if (username === null) return

            const password = password_input.get_value()
            if (password === null) return

            const request = PostJson('api/login/', BuildQuery({
                username: username,
                password: password,
            }), () => {}, response => {

                if (response === true) {
                    application.variables.session_user = {
                        username: username,
                    }
                    options.login()
                    return
                }

                if (response === 'LOGIN_INVALID') {
                    error('Invalid username or password')
                    abort = () => {
                        form.removeChild(error_element)
                    }
                    return
                }

            })

            abort = () => {
                request.abort()
            }

        },
    }, [
        Element({ className: 'LoginPage_Form-icon' }, [
            Element({
                className: 'LoginPage_Form-icon-content',
                style: { backgroundImage: 'url(img/icon/32.svg)' },
            }),
        ]),
        Element([
            Element({ className: 'LoginPage_Form-title' }, ['Eye']),
        ]),
        Element({ className: 'LoginPage_Form-subtitle' }, [
            'Reachability Monitoring',
        ]),
        username_input.element,
        password_input.element,
        Form_Button({ text: 'Log in' }),
    ])

    return {
        element: form,
        focus: username_input.focus,
    }

}
;
function LoginPage_Main (application, options) {

    const form = LoginPage_Form(application, options)

    return {
        element: Element({
            className: 'LoginPage_Main',
            style: { backgroundImage: 'url(img/login.svg)' },
        }, [
            Element({ className: 'LoginPage_Main-aligner' }),
            Element({ className: 'LoginPage_Main-content' }, [form.element]),
        ]),
        focus: form.focus,
    }

}
;
window.Main = variables => {

    function load (page) {
        current_page = page
        element.append(page.element)
        page.focus()
    }

    function showHomePage () {
        const home_page = HomePage_Main(application, {
            logout () {
                unload()
                showLoginPage()
            },
        })
        load(home_page)
    }

    function showLoginPage () {
        const login_page = LoginPage_Main(application, {
            login () {
                unload()
                showHomePage()
            },
        })
        load(login_page)
    }

    function unload () {
        element.removeChild(current_page.element)
    }

    let current_page
    const time_difference = Date.now() - variables.time

    const items = {
        scroll: [],
    }

    const application = {
        variables: variables,
        add (name, item) {
            items[name].push(item)
        },
        resource_url (path) {
            return variables.site_base + path
        },
        show_form (form) {

            function hide () {
                element.removeChild(dialog)
            }

            const dialog = Dialog({
                content: [form.element],
                close () {
                    hide()
                    form.destroy()
                },
            })
            element.append(dialog)
            form.focus()
            return hide

        },
        time_now () {
            return Date.now() - time_difference
        },
    }

    const element = Element({
        className: 'Main',
        onscroll () {
            items.scroll.forEach(item => {
                item(element)
            })
        },
    })

    document.body.append(element)
    if (variables.session_user) showHomePage()
    else showLoginPage()

}
;
const MonthNameShort = [
    'Jan', 'Feb', 'Mar',
    'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep',
    'Oct', 'Nov', 'Dec',
]
;
function Page_H1 (options) {
    return Element('h1', { className: 'Page_H1' }, [options.text])
}
;
function PasswordForm (options) {

    const current_password_input = Form_TextInput({
        type: 'password',
        name: 'current_password',
        label: 'Current password',
        required: true,
    })

    const new_password_input = Form_TextInput({
        next: true,
        type: 'password',
        name: 'new_password',
        label: 'New password',
        required: true,
    })

    const repeat_new_password_input = Form_TextInput({
        next: true,
        type: 'password',
        name: 'repeat_new_password',
        label: 'Repeat new password',
        required: true,
    })

    const form = Form({
        title: 'Change password',
        action: 'password/',
        logout: options.logout,
        items: [
            current_password_input.element,
            new_password_input.element,
            repeat_new_password_input.element,
            Form_Button({ text: 'Save' }),
        ],
        submit () {

            const current_password = current_password_input.get_value()
            if (current_password === null) return

            const new_password = new_password_input.get_value()
            if (new_password === null) return

            const repeat_new_password = repeat_new_password_input.get_value()
            if (repeat_new_password === null) return

            if (new_password !== repeat_new_password) {
                form.error('New passwords doesn\'t match')
                return
            }

            return {
                current_password: current_password,
                new_password: new_password,
            }

        },
        done (response) {

            if (response === true) {
                options.done()
                return
            }

            if (response === 'CURRENT_PASSWORD_INVALID') {
                form.error('Invalid current password')
                return
            }

        },
    })

    return {
        destroy: form.destroy,
        element: form.element,
        focus: current_password_input.focus,
    }

}
;
function Period_Chart (filter, reads) {

    const width_ratio = (() => {
        let width_ratio = (1000 * 60) / (filter.to - filter.from)
        while (width_ratio < 0.005) width_ratio *= 2
        return width_ratio
    })()

    return Element({ className: 'Period_Chart' }, reads.map(read => {

        const left_ratio = (read.time * 1000 - filter.from) / (filter.to - filter.from)

        return Element({
            className: 'Period_Chart-item',
            style: {
                left: left_ratio * 100 + '%',
                width: 'calc(' + (width_ratio * 100) + '% + 1px)',
            },
        }, [
            Element({ className: 'Period_Chart-item-fail' }),
            Element({
                className: 'Period_Chart-item-success',
                style: { height: read.result_success * 100 + '%' },
            }),
        ])

    }))

}
;
function Period_Item (response, filter, url, agents) {
    return Element({ className: 'Period_Item' }, [
        Element({ className: 'Period_Item-title' }, [url]),
        Element({
            className: 'Period_Item-content',
        }, response.agents.map((agent, index) => (
            Period_ItemLine(filter, agent, agents[agent.id], index)
        ))),
    ])
}
;
function Period_ItemLine (filter, agent, reads, index) {
    return Element({
        className: 'Period_ItemLine' + (index === 0 ? '' : ' next'),
    }, [
        Element({ className: 'Period_ItemLine-name' }, [agent.name]),
        Period_Chart(filter, reads),
    ])
}
;
function Period_Main (options) {

    const filter = options.filter

    const request = PostJson('api/fetch/', BuildQuery({
        from: Math.floor(filter.from / 1000),
        to: Math.floor(filter.to / 1000),
    }), () => {}, response => {

        if (response === 'LOG_IN') {
            options.logout()
            return
        }

        const urls = Object.create(null)
        response.checks.forEach(check => {
            urls[check.url] = {}
            response.agents.forEach(agent => {
                urls[check.url][agent.id] = []
            })
        })
        response.reads.forEach(item => {
            const agent_map_items = urls[item.url]
            if (agent_map_items === undefined) return
            agent_map_items[item.agent_id].push(item)
        })

        options.done({
            element: Element({
                className: 'Period_Main',
            }, Object.keys(urls).map(url => (
                Period_Item(response, filter, url, urls[url])
            ))),
            destroy () {
                request.abort()
            },
        })

    })

    return () => {
        request.abort()
    }

}
;
function PeriodSelect_Days (application, number) {
    return {
        text: number + 'd',
        update (render) {

            function next () {

                const to_date = new Date(application.time_now())
                to_date.setUTCMilliseconds(0)
                to_date.setUTCSeconds(0)

                const from_date = new Date(to_date.getTime())
                from_date.setUTCDate(from_date.getUTCDate() - number)

                timeout = setTimeout(next, 1000 * 60 * 60)

                render({
                    from: from_date.getTime(),
                    to: to_date.getTime(),
                })

            }

            let timeout
            next()

            return () => {
                clearTimeout(timeout)
            }

        },
    }
}
;
function PeriodSelect_Main (application, options) {

    const right_element = Element({ className: 'PeriodSelect_Main-right' })

    const menu = PeriodSelect_Menu(application, {
        update (filter) {
            while (right_element.lastChild) {
                right_element.lastChild.remove()
            }
            right_element.append(Element({
                className: 'PeriodSelect_Main-right-time',
            }, [FormatTime(filter.from)]))
            right_element.append(Element({
                className: 'PeriodSelect_Main-right-separator',
            }, [' - ']))
            right_element.append(Element({
                className: 'PeriodSelect_Main-right-time',
            }, [FormatTime(filter.to)]))
            options.update(filter)
        },
    })

    const left_element = Element({ className: 'PeriodSelect_Main-left' })
    left_element.append(menu.element)

    const element = Element({ className: 'PeriodSelect_Main' }, [
        left_element,
        right_element,
    ])

    application.add('scroll', main_element => {
        if (main_element.scrollTop === 0) element.classList.remove('scroll')
        else element.classList.add('scroll')
    })

    return {
        element,
        init: menu.init,
    }

}
;
function PeriodSelect_Menu (application, options) {

    function add (item_options) {

        function deselect () {
            destroy()
            button.classList.remove('active')
        }

        function select () {
            destroy = item_options.update(options.update)
            button.classList.add('active')
            deselect_selected = deselect
            button_node.nodeValue = item_options.text
            hide()
        }

        let destroy

        const button = Element('button', {
            className: 'PeriodSelect_Menu-item',
            onclick () {
                deselect_selected()
                select()
            },
        }, [item_options.text])

        menu_element.append(button)
        items.push(select)

    }

    function hide () {
        visible = false
        button.classList.remove('active')
        menu_element.classList.add('hidden')
        removeEventListener('focus', window_focus, true)
        removeEventListener('mousedown', window_mousedown)
    }

    function window_focus (e) {
        const target = e.target
        if (target === window || target === document) return
        if (element.contains(target)) return
        hide()
    }

    function window_mousedown (e) {
        if (element.contains(e.target)) return
        hide()
    }

    let deselect_selected
    let visible = false
    const items = []

    const button_node = document.createTextNode('button1')

    const button = Element('button', {
        className: 'PeriodSelect_Menu-button',
        style: {
            backgroundImage: 'url(' + application.resource_url('img/dark/point-bottom.svg') + ')',
        },
        onclick () {

            if (visible) {
                hide()
                return
            }

            visible = true
            button.classList.add('active')
            menu_element.classList.remove('hidden')
            addEventListener('focus', window_focus, true)
            addEventListener('mousedown', window_mousedown)

        },
    }, [button_node])

    const menu_element = Element({
        className: 'PeriodSelect_Menu-menu hidden',
    })
    add(PeriodSelect_OneHour(application))
    add(PeriodSelect_Days(application, 1))
    add(PeriodSelect_Days(application, 7))

    const element = Element({ className: 'PeriodSelect_Menu' }, [
        button,
        menu_element,
    ])

    return {
        element,
        init: items[0],
    }

}
;
function PeriodSelect_OneHour (application) {
    return {
        text: '1h',
        update (render) {

            function next () {

                const to_date = new Date(application.time_now())
                to_date.setUTCMilliseconds(0)
                to_date.setUTCSeconds(0)

                const from_date = new Date(to_date.getTime())
                from_date.setUTCHours(from_date.getUTCHours() - 1)

                timeout = setTimeout(next, 1000 * 60)

                render({
                    from: from_date.getTime(),
                    to: to_date.getTime(),
                })

            }

            let timeout
            next()

            return () => {
                clearTimeout(timeout)
            }

        },
    }
}
;
function PostJson (url, formData, errorCallback, loadCallback) {
    const request = new XMLHttpRequest
    request.open('post', url)
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
    request.responseType = 'json'
    request.send(formData)
    request.onerror = errorCallback
    request.onload = () => {
        if (request.status !== 200) {
            errorCallback()
            return
        }
        loadCallback(request.response)
    }
    return request
}
;
function Top (application, options) {

    let abort = () => {}

    return {
        element: Element({
            className: 'Top',
            style: { backgroundImage: 'url(img/top.svg)' },
        }, [
            Element({ className: 'Top-title' }, ['Eye']),
            Element({ className: 'Top-right' }, [
                TopButton({
                    icon: 'user',
                    text: 'Change username',
                    click () {
                        const hide = application.show_form(UsernameForm(application, {
                            done () {
                                hide()
                            },
                            logout () {
                                hide()
                                abort()
                                options.logout()
                            },
                        }))
                    },
                }),
                TopButton({
                    icon: 'key',
                    text: 'Change password',
                    click () {
                        const hide = application.show_form(PasswordForm({
                            done () {
                                hide()
                            },
                            logout () {
                                hide()
                                abort()
                                options.logout()
                            },
                        }))
                    },
                }),
                TopButton({
                    icon: 'arrow-right-out',
                    text: 'Log out',
                    click () {
                        abort()
                        const request = PostJson('api/logout/', '', () => {}, response => {
                            if (response === true) options.logout()
                        })
                        abort = () => {
                            request.abort()
                        }
                    },
                }),
            ]),
        ]),
        destroy () {
            abort()
        },
    }

}
;
function TopButton (options) {
    return Element('button', {
        className: 'TopButton',
        onclick: options.click,
        style: { backgroundImage: 'url(img/white/' + options.icon + '.svg)' },
    }, [
        Element({ className: 'TopButton-tip' }, [
            Element({ className: 'TopButton-tip-arrow' }),
            options.text,
        ]),
    ])
}
;
function TwoDigitPad (n) {
    let s = n.toString()
    if (s.length === 1) s = '0' + s
    return s
}
;
function UsernameForm (application, options) {

    const username_input = Form_TextInput({
        name: 'username',
        label: 'Username',
        required: true,
        value: application.variables.session_user.username,
    })

    const current_password_input = Form_TextInput({
        next: true,
        type: 'password',
        name: 'current_password',
        label: 'Current password',
        required: true,
    })

    const form = Form({
        title: 'Change username',
        action: 'username/',
        logout: options.logout,
        items: [
            username_input.element,
            current_password_input.element,
            Form_Button({ text: 'Save' }),
        ],
        submit () {

            const username = username_input.get_value()
            if (username === null) return

            const current_password = current_password_input.get_value()
            if (current_password === null) return

            return {
                username: username,
                current_password: current_password,
            }

        },
        done (response, params) {

            if (response === true) {
                application.variables.session_user.username = params.username
                options.done()
                return
            }

            if (response === 'CURRENT_PASSWORD_INVALID') {
                form.error('Invalid current password')
                return
            }

        },
    })

    return {
        destroy: form.destroy,
        element: form.element,
        focus: username_input.focus,
    }

}
;

})()