<?php

function mysqli_select_query_assoc (
    $mysqli, $table, $where, $order_by = null, $page = null) {

    $sql = "select * from `" . $mysqli->real_escape_string($table) . "`" .
        ' ' . mysqli_where($mysqli, $where) .
        ($order_by === null ? '' : " order by $order_by") .
        ($page === null ? '' : " limit $page[limit] offset $page[offset]");

    return mysqli_query_assoc($mysqli, $sql);

}
