<?php

function request_uint ($name, $nullable = false) {
    $value = request_string($name);
    if ($nullable && $value === '') return;
    return abs((int)$value);
}
