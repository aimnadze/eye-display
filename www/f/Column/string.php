<?php

namespace Column;

function string ($length) {
    return [
        'type' => "varchar($length)",
        'charset' => 'utf8mb4',
        'collate' => 'utf8mb4_general_ci',
    ];
}
