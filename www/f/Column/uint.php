<?php

namespace Column;

function uint ($primary = false) {
    return [
        'type' => 'bigint(20) unsigned',
        'primary' => $primary,
    ];
}
