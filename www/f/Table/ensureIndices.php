<?php

namespace Table;

function ensureIndices ($mysqli, $definition, &$output) {

    if (!array_key_exists('indices', $definition)) return;

    $table_name = $definition['name'];
    $existing_indices = \InformationSchema\Index\OnTable\index($table_name);

    foreach ($definition['indices'] as $index) {

        $found = false;
        foreach ($existing_indices as $i => $existing_index) {
            if ($index !== $existing_index) continue;
            unset($existing_indices[$i]);
            $found = true;
            break;
        }

        if ($found) continue;

        $sql = 'alter table `' . $mysqli->real_escape_string($table_name) . '`' .
            ' add index (' . join(', ', array_map(function ($column) use ($mysqli) {
                return '`' . $mysqli->real_escape_string($column) . '`';
            }, $index)) . ')';

        mysqli_safe_query($mysqli, $sql);

        $output .= "SQL: $sql\n";

    }

    if ($existing_indices) {
        foreach ($existing_indices as $index) {
            $output .= "    $table_name: Index remaining: " . join(', ', $index) . "\n";
        }
    }

}
