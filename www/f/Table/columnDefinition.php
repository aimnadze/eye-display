<?php

namespace Table;

function columnDefinition ($column) {

    $sql = $column['type'];

    if (array_key_exists('charset', $column)) {
        $sql .= " character set $column[charset]" .
            " collate $column[collate]";
    }

    if (array_key_exists('primary', $column) && $column['primary'] === true) {
        $sql .= ' auto_increment primary key';
    } elseif (!array_key_exists('nullable', $column) ||
        $column['nullable'] !== true) {

        $sql .= ' not null';

    }

    return $sql;

}
