<?php

namespace Table;

function ensure ($mysqli, $definition) {

    $table_name = $definition['name'];
    $columns = $definition['columns'];

    $table = \InformationSchema\Table\get($table_name);

    $output = '';

    if ($table) {

        $existing_columns = \InformationSchema\Column\OnTable\index($table_name);

        $remaining_columns = [];
        foreach ($existing_columns as $existing_column) {

            $name = $existing_column['COLUMN_NAME'];
            if (!array_key_exists($name, $columns)) {
                $remaining_columns[] = $name;
                continue;
            }

            $existing_type = $existing_column['COLUMN_TYPE'];
            $existing_nullable = $existing_column['IS_NULLABLE'] === 'YES';
            $existing_primary = $existing_column['COLUMN_KEY'] === 'PRI';
            $existing_increment = $existing_column['EXTRA'] === 'auto_increment';
            $existing_charset = $existing_column['CHARACTER_SET_NAME'];
            $existing_collate = $existing_column['COLLATION_NAME'];

            $column = $columns[$name];
            $type = $column['type'];
            $primary = array_key_exists('primary', $column) &&
                $column['primary'] === true;
            $nullable = array_key_exists('nullable', $column) &&
                $column['nullable'] === true;
            unset($columns[$name]);

            if (array_key_exists('charset', $column)) {
                $charset = $column['charset'];
                $collate = $column['collate'];
            } else {
                $charset = $collate = null;
            }

            if ($primary) $primary_ok = $existing_primary && $existing_increment;
            else $primary_ok = !$existing_primary && !$existing_increment;

            if ($type === $existing_type &&
                $nullable === $existing_nullable &&
                $charset === $existing_charset &&
                $collate === $existing_collate && $primary_ok) continue;

            if ($existing_primary) {
                $output .= dropPrimaryKey($mysqli, $table_name);
            }

            $output .= editColumn($mysqli, $table_name, $name, $column);

        }

        $output .= addColumns($mysqli, $table_name, $columns);

        foreach ($remaining_columns as $column) {
            $output .= "    $table_name: Column remaining: $column\n";
        }

    } else {
        $output .= create($mysqli, $table_name, $columns);
    }

    ensureIndices($mysqli, $definition, $output);

    return $output;

}
