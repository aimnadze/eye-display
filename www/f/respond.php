<?php

function respond ($response) {
    ob_start('ob_gzhandler');
    header('Content-Type: application/json');
    die(json_encode($response));
}
