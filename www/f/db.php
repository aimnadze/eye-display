<?php

function db () {
    static $db;
    if ($db === null) $db = new Db;
    return $db;
}
