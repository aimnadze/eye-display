<?php

function mysqli_where_conditions ($mysqli, $where) {
    $conditions = [];
    foreach ($where as $key => $value) {

        if (is_int($key)) {
            $conditions[] = "($value)";
            continue;
        }

        $conditions[] = '(`' . $mysqli->real_escape_string($key) . '`' .
            ' = ' . mysqli_escape($mysqli, $value) . ')';

    }
    return $conditions ? join(' and ', $conditions) : '1';
}
