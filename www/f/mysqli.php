<?php

function mysqli () {

    static $mysqli;
    if ($mysqli !== null) return $mysqli;

    mysql_config($host, $username, $password, $database);

    $fatal = function ($text) {
        fatal("MySQL connect error: $text");
    };

    try {
        $mysqli = @new mysqli($host, $username, $password, $database);
    } catch (mysqli_sql_exception $e) {
        $fatal($e->getMessage());
    }

    if ($mysqli->connect_errno !== 0) {
        fatal("MySQL connect error: $mysqli->connect_error");
    }

    $mysqli->set_charset('utf8');

    mysqli_safe_query($mysqli, "set @@sql_mode = ''");

    return $mysqli;

}
