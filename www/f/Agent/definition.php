<?php

namespace Agent;

function definition () {
    return [
        'name' => 'agent',
        'columns' => [
            'id' => \Column\uint(true),
            'name' => \Column\string(32),
            'password_hash' => \Password\column(),
        ],
    ];
}
