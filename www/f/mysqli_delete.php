<?php

function mysqli_delete ($mysqli, $table, $where) {

    $sql = "delete from `" . $mysqli->real_escape_string($table) . "`" .
        ' ' . mysqli_where($mysqli, $where);

    mysqli_safe_query($mysqli, $sql);

    return $mysqli->affected_rows;

}
