<?php

function register_autoload () {
    spl_autoload_register(function ($class) {
        include_once __DIR__ . "/../c/$class.php";
    });
}
