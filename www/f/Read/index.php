<?php

namespace Read;

function index ($agents, $filter) {

    $power = 0;
    $width_ratio = 60 / ($filter['to'] - $filter['from']);
    while ($width_ratio < 0.005) {
        $power++;
        $width_ratio *= 2;
    }

    $sql = 'select agent_id, url, min(insert_time) insert_time,' .
        ' avg(result_success) result_success' .
        ' from `read`' .
        ' ' . mysqli_where(mysqli(), [
            "insert_time >= $filter[from]",
            "insert_time < $filter[to]",
            mysqli_in(mysqli(), 'agent_id', array_map(function ($agent) {
                return $agent['id'];
            }, $agents)),
        ]) .
        " group by agent_id, url, insert_minute_$power";

    return mysqli_query_assoc(mysqli(), $sql);

}
