<?php

namespace Read;

function definition () {

    $columns = [
        'agent_id' => \Column\uint(),
        'expect_headers' => \Column\string(2048),
        'expect_status_code' => \Column\uint(),
        'id' => \Column\uint(true),
        'insert_time' => \Column\uint(),
        'result_error' => \Column\string(32),
        'result_headers' => \Column\string(2048),
        'result_status_code' => \Column\uint(),
        'result_success' => \Column\bool(),
        'url' => \Column\string(2048),
    ];

    $indices = [
        ['agent_id'],
        ['insert_time'],
        ['url'],
    ];

    for ($i = 0; $i < 16; $i++) {
        $columns["insert_minute_$i"] = \Column\uint();
        $indices[] = ["insert_minute_$i"];
    }

    return [
        'name' => 'read',
        'columns' => $columns,
        'indices' => $indices,
    ];

}
