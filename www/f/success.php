<?php

function success ($item, $check) {

    $result = $item['result'];
    if ($result['error'] !== '') return false;
    if ((int)$check['expect_status_code'] !== $result['status_code']) return false;

    foreach (json_decode($check['expect_headers'], true) as $key => $value) {
        if (!array_key_exists($key, $result['headers'])) return false;
        if ($result['headers'][$key] !== $value) return false;
    }

    return true;

}
