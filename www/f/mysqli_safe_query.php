<?php

function mysqli_safe_query ($mysqli, $sql) {

    $fatal = function ($text) use ($sql) {
        fatal("MySQL query error: $text; SQL: " . json_encode($sql));
    };

    try {
        $result = $mysqli->query($sql);
    } catch (mysqli_sql_exception $e) {
        $fatal($e->getMessage());
    }

    if ($result === false) $fatal($mysqli->error);

    return $result;

}
