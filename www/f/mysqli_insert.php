<?php

function mysqli_insert ($mysqli, $table, $values) {

    $columns = [];
    $sql_values = [];
    foreach ($values as $key => $value) {
        $columns[] = "`" . $mysqli->real_escape_string($key) . "`";
        $sql_values[] = mysqli_escape($mysqli, $value);
    }

    $sql = "insert into `" . $mysqli->real_escape_string($table) . "`" .
        ' (' . join(', ', $columns) . ')' .
        ' values (' . join(', ', $sql_values) . ')';

    mysqli_safe_query($mysqli, $sql);

    return $mysqli->insert_id;

}
