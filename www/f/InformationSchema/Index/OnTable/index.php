<?php

namespace InformationSchema\Index\OnTable;

function index ($table_name) {

    mysql_config($host, $username, $password, $database);

    $mysqli = mysqli();

    $sql = 'select * from information_schema.statistics' .
        " where table_schema = '" . $mysqli->real_escape_string($database) . "'" .
        " and table_name = '" . $mysqli->real_escape_string($table_name) . "'" .
        " and index_name != 'PRIMARY'" .
        ' order by index_name, seq_in_index';

    $rows = mysqli_query_assoc($mysqli, $sql);

    if (!$rows) return [];

    return \InformationSchema\Index\build($rows)[$table_name];

}
