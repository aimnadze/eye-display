<?php

namespace InformationSchema\Index;

function build ($rows) {
    $result = [];
    foreach ($rows as $row) {

        $table_name = $row['TABLE_NAME'];
        if (!array_key_exists($table_name, $result)) {
            $result[$table_name] = [];
        }

        $index_name = $row['INDEX_NAME'];
        if (!array_key_exists($index_name, $result[$table_name])) {
            $result[$table_name][$index_name] = [];
        }

        $result[$table_name][$index_name][] = $row['COLUMN_NAME'];

    }
    return $result;
}
