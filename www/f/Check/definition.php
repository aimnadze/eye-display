<?php

namespace Check;

function definition () {
    return [
        'name' => 'check',
        'columns' => [
            'expect_headers' => \Column\string(2048),
            'expect_status_code' => \Column\uint(),
            'id' => \Column\uint(true),
            'url' => \Column\string(1024),
        ],
    ];
}
