<?php

namespace ErrorPage;

function forbidden () {

    $description = 'Access to the page ' .
        '<em>' . htmlspecialchars($_SERVER['REQUEST_URI']) . '</em> denied.';

    create(403, 'Forbidden', $description);

}
