<?php

namespace ErrorPage;

function internalServerError ($reference) {

    $description = 'The page ' .
        '<em>' . htmlspecialchars($_SERVER['REQUEST_URI']) . '</em>' .
        ' has failed to load.<br /><br />' .
        '<div style="font-size: 12px; color: #7f7f7f">' .
            "Reference: $reference" .
        '</div>';

    create(500, 'Internal Server Error', $description);

}
