<?php

namespace ErrorPage;

function notFound () {

    $description = 'The page ' .
        '<em>' . htmlspecialchars($_SERVER['REQUEST_URI']) . '</em>' .
        ' was not found.';

    create(404, 'Not Found', $description);

}
